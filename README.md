# Local storage & Session Storage

### Vad är det för något?
Det är en liten SQLite databas som är inbyggt i webbläsare där man kan spara data som är bundet till sin domän.

Datan sparas i key=value pairs ('name'='Mike Johansson') och value måste vara en sträng. Vill man spara mer data kan man göra det med `JSON` men man måste göra om det till en sträng med `JSON.stringify()` först. Vill man sedan läsa `JSON` data från en storage måste man köra `JSON.parse()`. Man kan även använda andra datatyper men man måste konvertera dessa till en sträng först innan man lägger till det i `storage`.

Exempel spara key=value pair
```js
localStorage.setItem('name', 'Mike Johansson')

sessionStorage.setItem('name', 'Mike Johansson')
```

Exempel spara key=json pair
```js
const user = { firstName: 'Mike', lastName: 'Johansson', age: '24'};

localStorage.setItem('user', JSON.stringify(user));

sessionStorage.setItem('user', JSON.stringify(user))
```

Exempel läs key=value pair
```js
localStorage.getItem('name')
// 'Mike Johansson'
sessionStorage.getItem('name')
// 'Mike Johansson'
```

Exempel läs key=json pair
```js
const data = localStorage.getItem('user')
JSON.parse(data)
// { firstName: 'Mike', lastName: 'Johansson', age: '24' }

const data = sessionStorage.getItem('user')
JSON.parse(data)
// { firstName: 'Mike', lastName: 'Johansson', age: '24' }
```
---

### Vad är skillnaden mellan `localStorage` och `sessionStorage`?

Båda två kan spara exakt samma data, den enda skillnaden är hur länge datan sparas. I `localStorage` sparas datan tills vi (utvecklarna) eller användaren själva tar bort det från webbläsaren. Så data som sparas i `localStorage` finns kvar efter att användarna stängt ner webbläsaren.

`sessionStorage`, som man hör på namnet, sparar datan under en session. En session är en tab och så länge man har tabben igång så finns sessionen kvar. Varje ny tab startar en ny session, så det går inte att dela information mellan tabbarna även om man är på samma domän.

---
### Vad är bra att spara i `localStorage`?

I `localStorage` är det vanligt att man sparar inställningar man har på sin sida. Om man kan ändra teman, färger, om en sidebar ska vara utfälld eller infälld och liknande. Då behöver inte användare göra om dessa inställningar varje gång de kommer tillbaka till eran sida. Man brukar även spara vilket språk en användare föredrar att ens sida ska visas på.

Om man gör spel kan poäng och state vara en bra grej att spara här om man vill komma ihåg det till nästa gång en användare ska spela igen.

---
### Vad är bra att spara i `sessionStorage`?

I `sessionStorage` brukar man spara saker som inte behöver vara kvar när användare stänger ner sidan. I vissa fall kan en kundvagn vara rätt vanligt här eftersom att den informationen i många fall inte behöver sparas efter att användaren stängt ner sidan. Vill man dock att olika tabbar ska dela kundvagn måste man använda sig av `localStorage` istället.

Om man vill spara actions en användare har gjort kan man spara det här också. I så fall skulle man kunna ha en undo knapp som bara går tillbaka i tiden med hjälp av denna datan.

---
### Kan man spara känslig information?

Kort svar: NEJ!

Långt svar: Man brukar avråda från att spara känslig information här eftersom att vem som helst som har tillgång till webbläsaren kan se vad som finns sparat. Men om man är tvungen (oftast har man byggt systemet fel från grunden om det ska behövas) så måste man göra det på ett säkert sätt genom kryptering, nyckelroteringar med mera. Allt blir bara onödigt komplicerat och det finns bättre sätt att spara känslig information på.
