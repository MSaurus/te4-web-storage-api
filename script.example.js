window.addEventListener('load', () => {
	const backgroundColorInput = document.querySelector('#background-color')
	const textColorInput = document.querySelector('#text-color')
	const fontSizeInput = document.querySelector('#font-size')

	if (localStorage.getItem('theme')) {
		const settings = JSON.parse(localStorage.getItem('theme'))
		const body = document.querySelector('body')

		if (settings.backgroundColor) {
			body.style.backgroundColor = settings.backgroundColor
			backgroundColorInput.value =settings.backgroundColor
		}

		if (settings.textColor) {
			body.style.color = settings.textColor
			textColorInput.value = settings.textColor
		}

		if (settings.fontSize) {
			const headings = document.querySelectorAll('h1')

			for (const heading of headings) {
				heading.style.fontSize = `${32 * parseInt(settings.fontSize, 10)}px`
			}

			body.style.fontSize = `${16 * parseInt(settings.fontSize, 10)}px`
			fontSizeInput.value = settings.fontSize
		}

	} else {
		const settings = { backgroundColor: null, textColor: null, fontSize: null }
		localStorage.setItem('theme', JSON.stringify(settings))
	}

	backgroundColorInput.addEventListener('change', changeBackgroundColor)
	textColorInput.addEventListener('change', changeTextColor)
	fontSizeInput.addEventListener('change', changeFontSize)
})

function changeBackgroundColor(event) {
	const body = document.querySelector('body')
	const oldTheme = JSON.parse(localStorage.getItem('theme'))
	const newTheme = { backgroundColor: event.target.value, textColor: oldTheme.textColor, fontSize: oldTheme.fontSize }

	body.style.backgroundColor = event.target.value
	localStorage.setItem('theme', JSON.stringify(newTheme))
}

function changeTextColor(event) {
	const body = document.querySelector('body')
	const oldTheme = JSON.parse(localStorage.getItem('theme'))
	const newTheme = { backgroundColor: oldTheme.backgroundColor, textColor: event.target.value, fontSize: oldTheme.fontSize }

	body.style.color = event.target.value
	localStorage.setItem('theme', JSON.stringify(newTheme))
}

function changeFontSize(event) {
	const headings = document.querySelectorAll('h1')
	const body = document.querySelector('body')
	const oldTheme = JSON.parse(localStorage.getItem('theme'))
	const newTheme = { backgroundColor: oldTheme.backgroundColor, textColor: oldTheme.textColor, fontSize: parseInt(event.target.value, 10) }

	for (const heading of headings) {
		heading.style.fontSize = `${32 * parseInt(event.target.value, 10)}px`
	}

	body.style.fontSize = `${16 * parseInt(event.target.value, 10)}px`
	localStorage.setItem('theme', JSON.stringify(newTheme))
}