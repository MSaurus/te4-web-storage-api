window.addEventListener('load', () => {
	const backgroundColorInput = document.querySelector('#background-color')
	const textColorInput = document.querySelector('#text-color')
	const fontSizeInput = document.querySelector('#font-size')

	if (localStorage.getItem('theme')) {
		const settings = JSON.parse(localStorage.getItem('theme'))
		const body = document.querySelector('body')

		if (settings.backgroundColor) {
			body.style.backgroundColor = settings.backgroundColor
			backgroundColorInput.value = settings.backgroundColor
		}
	} else {
		const settings = { backgroundColor: null, textColor: null, fontSize: null }
		localStorage.setItem('theme', JSON.stringify(settings))
	}

	backgroundColorInput.addEventListener('change', changeBackgroundColor)
})

function changeBackgroundColor(event) {
	const body = document.querySelector('body')
	const oldTheme = JSON.parse(localStorage.getItem('theme'))
	const newTheme = { backgroundColor: event.target.value, textColor: oldTheme.textColor, fontSize: oldTheme.fontSize}

	body.style.backgroundColor = event.target.value
	localStorage.setItem('theme', JSON.stringify(newTheme))
}